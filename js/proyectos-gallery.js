$('.proyectos-gallery__categories-container__category-list__category').on('click', function(){
  
  $('.proyectos-gallery__grid__proyecto').show();

  var category = this.id;
  var categoryArr = document.getElementsByClassName('proyectos-gallery__categories-container__category-list__category');
  var activeClass = 'proyectos-gallery__categories-container__category-list__category--active';

  [].forEach.call(categoryArr, function (element) {

    if(element.id === category) {
      element.classList.add(activeClass);
    } else if (element.classList.contains(activeClass)) {
      element.classList.remove(activeClass);
    }

  });

  if(category === "ver-todos") {
    return;
  }

  var pictureArr = document.getElementsByClassName('proyectos-gallery__grid__proyecto');

  [].forEach.call(pictureArr, function (element) {

    if(!element.classList.contains(category)) {
      element.style.display = "none";
    }
  });

});