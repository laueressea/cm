$('.proyectos-gallery__categories-container__category-list__category').on('click', function(){
  
  $('.proyectos-gallery__grid__proyecto').show();

  var category = this.id;

  if(category === "ver-todos") {
    return;
  }

  var pictureArr = document.getElementsByClassName('proyectos-gallery__grid__proyecto');

  [].forEach.call(pictureArr, function (element) {

    if(!element.classList.contains(category)) {
      element.style.display = "none";
    }
  });
})