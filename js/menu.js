/* MODAL BUTTON ANIMATION */

var container = document.querySelector(".header__logo-and-menu-container__menu-icon");
var oval = document.querySelector(".header__logo-and-menu-container__menu-icon__oval");

function setOval() {
  TweenMax.set(oval, {clearProps: 'all'});
}

function positionOval(e) {

  var rect = container.getBoundingClientRect(); 
 
  var relX = e.offsetX;
  var relY = e.offsetY;

  TweenMax.to(oval, 0.3, { x: relX, y: relY, scale: 1.2, ease:Power4.easeOut })
  TweenMax.set(oval, {
    xPercent: -50,
    yPercent: -50
  })
}

function magnetOval(e) {
  var rect = container.getBoundingClientRect();
  var center = {
    w: Math.round(rect.width * 0.5),
    h: Math.round(rect.height * 0.5)
}
      TweenMax.to(oval, 1, {
        x: center.w,
        y: center.h,
        ease: Back.easeInOut,
        scale: 1,
        onComplete: setOval
    } )
    TweenMax.set(oval, {
        xPercent: -50,
        yPercent: -50
      })
}

var posOval = function(e) {
    positionOval(e);
  };

var magOval = function(e) {
    magnetOval(e);
  };

function addAnim() {
  container.addEventListener("pointerenter", posOval, false);

  container.addEventListener("pointerleave", magOval, false);

  container.addEventListener("pointermove", posOval, false);
}

function removeAnim() {
  container.removeEventListener("pointerenter", posOval, false);

  container.removeEventListener("pointerleave", magOval, false);

  container.removeEventListener("pointermove", posOval, false);
}

var centerId;

function animateOval() {
  if(window.innerWidth >= 1200) {
    addAnim();    
  } else {
    removeAnim();
    clearTimeout(centerId);
    centerId = setTimeout(setOval, 300);
  }
}

$(document).ready(function(){
  animateOval();
});

var resizeId;

$(window).resize(function() {

    clearTimeout(resizeId);
    resizeId = setTimeout(animateOval, 500);

});

/* OPEN AND CLOSE MODAL */

const modal = document.querySelector('.menu-modal');
const menuIcon = document.querySelector('.header__logo-and-menu-container__menu-icon');
const closeText = document.querySelector('.header__logo-and-menu-container__menu-icon__text');
const ex = document.querySelector('.header__logo-and-menu-container__menu-icon__x');

menuIcon.addEventListener('click', function(e) {

  e.preventDefault();

  if(modal.classList.contains('menu-modal--shown')) {

    modal.classList.remove('menu-modal--shown');
    $('.header__logo-and-menu-container__menu-icon__text').html('menú');
    $('.header__logo-and-menu-container__menu-icon__oval__small-oval').show();
    $('.header__logo-and-menu-container__menu-icon__x').hide();

    if(window.innerWidth >= 1200) {

      closeText.style.pointerEvents = 'none';
      closeText.style.cursor = 'none';
      closeText.classList.remove('hover-shrink');
      ex.style.pointerEvents = 'none';
      addAnim();

    }

  } else {

    modal.classList.add('menu-modal--shown');
    $('.header__logo-and-menu-container__menu-icon__text').html('cerrar');
    $('.header__logo-and-menu-container__menu-icon__x').show();
    $('.header__logo-and-menu-container__menu-icon__oval__small-oval').hide();
    
    if(window.innerWidth >= 1200) {

      closeText.style.pointerEvents = 'auto';
      closeText.style.cursor = 'pointer';
      closeText.classList.add('hover-shrink');
      ex.style.pointerEvents = 'auto';
      magnetOval();
      removeAnim(); 

    }
  }
})