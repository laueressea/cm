$(document).ready(function(){
  $('.header__slider').slick({
    autoplay: true,
    infinite: true,
    autoplaySpeed: 4000,
    arrows: false,
    dots: true,
    pauseOnHover: false
  });
});